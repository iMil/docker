My Own Trading Dashboard
========================

![motd](images/motd.png)

This _compose file_ puts together the pieces needed to create a custom dashboard to follow the evolution of your cryptocurrencies assets.

Truth be told, it could be used to graph and monitor pretty much anything, but crypto was the main target. I use it also to track my home machines temperatures and load average.

It links:

- [collectd][2] along with [collectd-ticker][1], a [collectd][2] plugin I wrote to retrieve cryptocurrencies values on various exchanges
- [collectd-exporter][3], a _prometheus_ / _collectd_ gateway
- [prometheus][4], a popular time-series database
- [grafana][5] to visualize the results

This project is very similar to [cifstack][6], also by me, but it's cleaner, uses [prometheus][4] and is more flexible.

⚠  the `prometheus` directory must belong to the `nobody` user in order for the `prometheus` container to start.

## 3rd party monitoring

If you wish to use _motd_ to graph anything not coming from the _docker-compose_ network, you can do it by using the _collectd-exporter_ which listens to port **25827/udp** instead of the standard `collectd` port _25826_. Just point a regular `collectd` daemon to this new port:

```
<Plugin network>
	Server "192.168.1.1" "25827"
</Plugin>
```

As the `docker-compose.yml` exports port _25827_ for container _collectd-exporter_, it will redistribute the data to _prometheus_ when the latter will query it, and it will provide a valid set of data.

## Usage

The one configuration you definitely want to modify is `ticker.json`, where the currencies you want to follow are listed, along with the URL where to get the values from. Please refer to [collectd-ticker][1] repository in order to correctly fill this file.

Once done, fire up `docker-compose`:

```
$ docker-compose up
```

You may want to start it _without_ the `-d` flag at first, just to see if all containers start without errors.

Point your browser to `http://<docker_host>:3000`, `docker_host` being the machine hosting the `docker` containers, ofter `localhost`. The login and password are _Grafana's_ default `admin` / `admin`.

Then add a _datasource_ with `Prometheus` as the _Type_, `http://localhost:9090` as the _URL_ and `Browser` as the _Access_ method. You might have to wait a minute or so for the database to be filled with data, just retry.

![add datasource](images/datasource.png)

From there you should be able to create a _dashboard_ in order to _graph_ your assets by clicking the following icon:

![new dashboard](images/new_dashboard.png)

Then create a new _panel_ by clicking on this button:

![new panel](images/new_panel.png)

Now select the "Graph" type:

![graph](images/graph.png)

This will create an empty panel, click on its title and choose "Edit":

![edit panel](images/edit_panel.png)

The important thing here is to enter the right query, fortunately, _prometheus_ makes this task easy. Point a new browser window to the address `http://localhost:9090`, there you'll find a very basic web interface. Head to the `Graph` menu:

![prometheus graph](images/prom_graph.png)

Select `collectd_ticker_gauge` as the metric and click `Execute`, you'll then see all the available elements collected by _collectd-ticker_, copy paste the wanted _pair_, for example here `collectd_ticker_gauge{instance="collectd:9103",job="collectd",ticker="btcusd"}`:

![prometheus exec](images/prom_exec.png)

![pair](images/pair.png)

The `exported_instance` is useless in our use case.  
You may want to modify the graph's title in the _General_ section, as well as the unit in the _Axes_ tab.

And voila! Add all currencies you'd like to monitor, as well as all the metrics you want to display.

[1]: https://gitlab.com/iMil/collectd-ticker
[2]: https://collectd.org
[3]: https://github.com/prometheus/collectd_exporter
[4]: https://prometheus.io/
[5]: https://grafana.com/
[6]: https://gitlab.com/iMil/docker/tree/master/cifstack
